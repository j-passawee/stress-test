/* global Peer */

var config = {
  // port: 80,
  // host: 'api.tripeer.jp',
  port: 3000,
  host: 'localhost',
  path: '/peerjs',
  // Set API key for cloud server (you don't need this if you're running your own.
  // key: this.key,

  // Set highest debug level (log everything!).
  debug: 3,
  // Set it to false because of:
  // > PeerJS:  ERROR Error: The cloud server currently does not support HTTPS.
  // > Please run your own PeerServer to use HTTPS.
  secure: false,

  config: {
    iceServers: [
      'stun.l.google.com:19302'
    ]
  }
};
// api.tripeer.com
// var id1 = '57f31d7ff5d56e04e1086c84';
// var id2 = '57f31e07f5d56e04e1086c85';

// localhost
var rand1 = Math.floor( Math.random() * 5000 ) ;
var rand2 = Math.floor( Math.random() * 5000 ) ;
var id1 = 'id'  + rand1;
var id2 = 'id'  + rand2;
var id3 = 'id3';
var id4 = 'id4';
var peer1;
var peer2;
// var peer3;
// var peer4;

function after() {
  if (peer1) peer1.disconnect();
  if (peer2) peer2.disconnect();
  // if (peer3) peer3.disconnect();
  // if (peer4) peer4.disconnect();
}

function before() {
  peer1 = new Peer(id1, config);
  peer2 = new Peer(id2, config);
  // peer3 = new Peer(id3, config);
  // peer4 = new Peer(id4, config);
}

var test3 = function(done) {
  peer1.on('RESOLVE', function(data) {
    console.log('RESOLVE peer1: ', data);
    if (done) done();
  });

  peer2.on('CALLING', function(data) {
    console.log('CALLING peer2: ', data);
    peer2.send('RESOLVE', data.srcId);
  });

  setTimeout(function() {
    console.log('send CALLING');
    peer1.send('CALLING', id2);
  }, 1500);
}

function runTest(test) {
  // new Promise(function(resolve) {
    before();
    test(function(){
      // after();
      console.log(test.name, 'successed');
      // resolve();
    });
  // });
}

// runTest(test1);
// runTest(test2);
runTest(test3);
// runTest(test4);
// runTest(test5);

  // .then(function(){
  //   return runTest(test2);
  // })
  // .catch(function(err) {
  //   console.error(err);
  // });
