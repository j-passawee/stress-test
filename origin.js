/* global Peer */

var config = {
  // port: 80,
  // host: 'api.tripeer.jp',
  port: 3000,
  host: 'localhost',
  path: '/peerjs',
  // Set API key for cloud server (you don't need this if you're running your own.
  // key: this.key,

  // Set highest debug level (log everything!).
  debug: 3,
  // Set it to false because of:
  // > PeerJS:  ERROR Error: The cloud server currently does not support HTTPS.
  // > Please run your own PeerServer to use HTTPS.
  secure: false,

  config: {
    iceServers: [
      'stun.l.google.com:19302'
    ]
  }
};
// api.tripeer.com
// var id1 = '57f31d7ff5d56e04e1086c84';
// var id2 = '57f31e07f5d56e04e1086c85';

// localhost
var id1 = 'id1';
var id2 = 'id2';
var id3 = 'id3';
var id4 = 'id4';
var peer1;
var peer2;
var peer3;
var peer4;

function after() {
  if (peer1) peer1.disconnect();
  if (peer2) peer2.disconnect();
  if (peer3) peer3.disconnect();
  if (peer4) peer4.disconnect();
}

function before() {
  peer1 = new Peer(id1, config);
  peer2 = new Peer(id2, config);
  peer3 = new Peer(id3, config);
  peer4 = new Peer(id4, config);
}

var test1 = function(done) {

  peer1.on('REJECT', function(data) {
    console.log('REJECT peer1: ', data);
    if (data.pattern === 'deny') {
      if (done) done();
    }
  });

  peer2.on('CALLING', function(data) {
    console.log('CALLING peer2: ', data);
    peer2.send('REJECT', data.srcId);
  });

  setTimeout(function() {
    console.log('send CALLING');
    peer1.send('CALLING', id2);
  }, 1500);
};

var test2 = function (done) {

  peer2.on('REJECT', function(data) {
    console.log('REJECT peer2: ', data);
    if (data.pattern === 'cancel') {
      if (done) done();
    }
  });

  peer2.on('CALLING', function(data) {
    console.log('CALLING peer2: ', data);
    // peer2.send('REJECT', data.srcId);
  });


  setTimeout(function() {
    console.log('send CALLING');
    peer1.send('CALLING', id2);
    setTimeout(function() {
      peer1.send('REJECT', id2);
    }, 1000);
  }, 1500);

};

var test3 = function(done) {
  console.log('peer4', peer4);
  peer1.on('RESOLVE', function(data) {
    console.log('RESOLVE peer1: ', data);
    if (done) done();
  });

  peer2.on('CALLING', function(data) {
    console.log('CALLING peer2: ', data);
    peer2.send('RESOLVE', data.srcId);
  });

  setTimeout(function() {
    console.log('send CALLING');
    peer1.send('CALLING', id2);
  }, 1500);
}

var test4 = function(done) {
  peer1.on('RESOLVE', function(data) {
    console.log('RESOLVE peer1: ', data);
    // if (done) done();
  });

  peer2.on('CALLING', function(data) {
    console.log('CALLING peer2: ', data);
    peer2.send('RESOLVE', data.srcId);
  });

  peer3.on('REJECT', function(data) {
    console.log('REJECT peer3: ', data);

    if (data.pattern === 'busy') {
      if (done) done();
    }
  });

  setTimeout(function() {
    console.log('send CALLING');
    peer1.send('CALLING', id2);
  }, 1500);

  setTimeout(function() {
    console.log('send CALLING');
    peer3.send('CALLING', id2);
  }, 2000);
}

var test5 = function (done) {

  peer1.on('REJECT', function(data) {
    console.log('REJECT peer1: ', data);
  });

  setTimeout(function() {
    peer2.disconnect();
  }, 100);

  setTimeout(function() {
    peer1.send('CALLING', id2);
    console.log('send CALLING');

    peer2 = new Peer(id2, config);

    peer2.on('STATUS', function(data) {
      console.log('STATUS peer2: ', data);
      if (done) done();
    });

    setTimeout(function() {
      console.log(peer2)
      peer2.send('STATUS', id2);
      console.log('send CALLING');
    }, 1000);
  }, 2000);


};


function runTest(test) {
  // new Promise(function(resolve) {
    before();
    test(function(){
      // after();
      console.log(test.name, 'successed');
      // resolve();
    });
  // });
}

// runTest(test1);
// runTest(test2);
runTest(test3);
// runTest(test4);
// runTest(test5);

  // .then(function(){
  //   return runTest(test2);
  // })
  // .catch(function(err) {
  //   console.error(err);
  // });
